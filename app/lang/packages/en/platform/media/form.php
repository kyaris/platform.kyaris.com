<?php

return [
	'file'         => 'Update or Change the File',
	'private'      => 'Who Can View',
	'private_help' => 'If set to public, then anyone can view this file.  Setting to private allows you to specify certain groups.'
];
