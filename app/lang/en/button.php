<?php
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Button Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used throughout the whole application.
	| You can change them to anything you want to customize your views to
	| better match your application.
	|
	*/

	'add'       => 'Add',
	'create'    => 'Create',
	'remove'    => 'Remove',
	'cancel'    => 'Cancel',
	'delete'    => 'Delete',
	'edit'      => 'Edit',
	'update'    => 'Save Changes',
	'submit'    => 'Submit',
	'save'      => 'Save',
	'enable'    => 'Enable',
	'disable'   => 'Disable',
	'install'   => 'Install',
	'uninstall' => 'Uninstall',
	'copy'      => 'Copy',
	'close'     => 'Close',
	'upload'    => 'Upload',

	'bulk' => [
		'delete'  => 'Delete Selected',
		'enable'  => 'Enable Selected',
		'disable' => 'Disable Selected',
	],

];
