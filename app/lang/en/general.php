<?php
/**
 * Part of the Platform application.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the 3-clause BSD License.
 *
 * This source file is subject to the 3-clause BSD License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://www.opensource.org/licenses/BSD-3-Clause
 *
 * @package    Platform
 * @version    2.0.0
 * @author     Cartalyst LLC
 * @license    BSD License (3-clause)
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return array(

	'error'        => 'Error',
	'success'      => 'Success',
	'warning'      => 'Warning',
	'yes'          => 'Yes',
	'no'           => 'No',
	'all'          => 'All',
	'enabled'      => 'Enabled',
	'disabled'     => 'Disabled',
	'installed'    => 'Installed',
	'uninstalled'  => 'Uninstalled',
	'actions'      => 'Actions',
	'bulk_actions' => 'Bulk Actions',
	'status'       => 'Status',

	'delete_record' => 'You are about to delete this record, do you want to continue?',

	'not_logged_in' => 'You are not logged in.',

	// Search, Filter & Pagination
	'no_results'    => 'No Results',
	'showing'       => 'Showing',
	'to'            => 'to',
	'in'            => 'in',
	'of'            => 'of',
	'entries'       => 'entries',
	'loading'       => 'Loading',
	'search'        => 'Search',
	'filters'       => 'Filters',
	'remove_filter' => 'Remove filter',
	'show_all'      => 'Show all',
	'show_enabled'  => 'Show enabled',
	'show_disabled' => 'Show disabled',
	'all_enabled'   => 'All Enabled',
	'all_disabled'  => 'All Disabled',

);
